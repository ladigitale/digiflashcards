import en from './en.json'
import it from './it.json'
import fr from './fr.json'

export default {
	en,
	it,
	fr
}
